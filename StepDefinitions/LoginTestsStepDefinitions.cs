﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using TechTalk.SpecFlow;

namespace Test_SpecFlowProject.StepDefinitions
{
    [Binding]
    public class LoginTestsStepDefinitions
    {
        private readonly IWebDriver driver;

        public LoginTestsStepDefinitions(IWebDriver driver)
        {
            this.driver = driver;
        }

        //Scenario: LoginPasswordAreValid
        [When(@"User clicks on the 'Enter' button")]
        public void WhenUserClicksOnEnterButton()
        {
            var enterbutton = driver.FindElement(By.CssSelector("a.default-button:nth-child(2)"));
            enterbutton.Click();

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(70);
        }

        [When(@"User enters valid email")]
        public void WhenUserEntersValidEmail()
        {
            var emailfield = driver.FindElement(By.XPath("/html/body/div/div/div/form/div/label/input"));
            emailfield.Clear();
            emailfield.SendKeys("anniebrusentsova@gmail.com");

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
        }

        [When(@"User clicks on 'Continue' button")]
        public void WhenUserClicksOnContinueButton()
        {
            var continuebutton = driver.FindElement(By.XPath("/html/body/div/div/div/form/button"));
            continuebutton.Click();          
        }

        [Then(@"User see Password field")]
        public void ThenUserSeePasswordField()
        {           
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(100)); // Set explicit wait
            IWebElement element = wait.Until(ExpectedConditions.ElementExists(By.XPath("/html/body/div/div/div/form/button[2]")));
        }

        [When(@"User enters valid password")]
        public void WhenUserEntersValidPassword()
        {
            var passwordfield = driver.FindElement(By.XPath("/html/body/div/div/div/form/div/label/input"));
            passwordfield.Clear();
            passwordfield.SendKeys("qwerty4321");
        }

        [When(@"User clicks on 'Login' button")]
        public void WhenUserClicksOnLoginButton()
        {
            var loginenterbutton = driver.FindElement(By.XPath("/html/body/div/div/div/form/button[2]"));
            loginenterbutton.Click();
        }

        [Then(@"User logged in with valid credentials")]
        public void ThenUserLoggedInWithValidCredentials()
        {
            string expectedText = "Ваше направление";
            var directiontext = driver.FindElement(By.XPath("/html/body/div[1]/main/div[1]/div/div[1]/div[2]/div[1]/h3"));
            string actualText = directiontext.Text;
            Assert.AreEqual(expectedText, actualText);
        }

        //Scenario: PasswordIsInvalid
        [When(@"User enters invalid password")]
        public void WhenUserEntersInvalidPassword()
        {
            var passwordfield = driver.FindElement(By.XPath("/html/body/div/div/div/form/div/label/input"));
            passwordfield.Clear();
            passwordfield.SendKeys("1111");
        }

        [Then(@"User see error message under password field")]
        public void ThenUserSeeErrorMessageUnderPasswordField()
        {
            string expectedText = "Пароль должен быть больше 8 символов";
            var errortext = driver.FindElement(By.XPath("/html/body/div/div/div/form/div/p/span"));
            string actualText = errortext.Text;
            Assert.AreEqual(expectedText, actualText);
        }
        

    }
}
