﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace Test_SpecFlowProject.StepDefinitions
{
    [Binding]
    public sealed class LocalizationTestsStepDefinitions
    {
        private readonly IWebDriver driver;

        public LocalizationTestsStepDefinitions(IWebDriver driver)
        {
            this.driver = driver;
        }

        //Scenario: CheckRUSVersion
        [Given("User is on the Promo page")]
        public void GivenUserOnPromoPage()
        {          
            string expectedText = "Добро пожаловать в howtomove";
            var maintitletext = driver.FindElement(By.XPath("/html/body/div/main/section[1]/div/p[1]"));
            string actualText = maintitletext.Text;
            Assert.AreEqual(actualText, expectedText);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(70);                   
        }

        [When("User clicks on the 'Start' button")]
        public void WhenButtonIsClicked()
        {
            var startbutton = driver.FindElement(By.CssSelector("a.button--blue:nth-child(2)"));          
            startbutton.Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(70);
        }

        [Then("User should be redirected to 'Main' page")]
        public void ThenUserRedirectedToMainPage()
        {
            string expectedText = "Ваше направление";
            var directiontext = driver.FindElement(By.XPath("/html/body/div[1]/main/div[1]/div/div[1]/div[2]/div[1]/h3"));
            string actualText = directiontext.Text;
            Assert.AreEqual(actualText, expectedText);
        }

        //Scenario: CheckENVersion

        [When("User clicks on the Language dropdown")]
        public void WhenUserClicksOnLanguageDropdown()
        {
            var changelanguagebutton = driver.FindElement(By.XPath("/html/body/div[1]/main/header/div/div/div[1]/div/div/button"));
            changelanguagebutton.Click();
        }

        [When("User selects the EN language")]
        public void WhenUserSelectsENLanguage()
        {
            var selectENlanguage = driver.FindElement(By.CssSelector("div.active > div:nth-child(2) > button:nth-child(1)"));
            selectENlanguage.Click();

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(70);
        }

        [Then("EN language is selected")]
        public void ThenENLanguageSelected()
        {
            string expectedText = "Your direction";
            var directiontext = driver.FindElement(By.XPath("/html/body/div[1]/main/div[1]/div/div[1]/div[2]/div[1]/h3"));
            string actualText = directiontext.Text;
            Assert.AreEqual(expectedText, actualText);
        }

        //Scenario: CheckUAVersion
        [When(@"User selects the UA language")]
        public void WhenUserSelectsTheUALanguage()
        {
            var selectUAlanguage = driver.FindElement(By.CssSelector("div.active > div:nth-child(2) > button:nth-child(3)"));
            selectUAlanguage.Click();

            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(70);
        }

        [Then(@"UA language is selected")]
        public void ThenUAlanguageIsSelected()
        {
            string expectedText = "Ваш напрямок";
            var directiontext = driver.FindElement(By.XPath("/html/body/div[1]/main/div[1]/div/div[1]/div[2]/div[1]/h3"));
            string actualText = directiontext.Text;
            Assert.AreEqual(expectedText, actualText);
        }


    }
}