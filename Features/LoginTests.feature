﻿Feature: LoginTests


@LoginTests
Scenario Outline: LoginPasswordAreValid
	Given User is on the Promo page
	When User clicks on the 'Start' button
	When User clicks on the 'Enter' button
	When User enters valid email
	When User clicks on 'Continue' button
	Then User see Password field
	When User enters valid password
	When User clicks on 'Login' button
	Then User logged in with valid credentials


Scenario Outline: PasswordIsInvalid
	Given User is on the Promo page
	When User clicks on the 'Start' button
	When User clicks on the 'Enter' button
	When User enters valid email
	When User clicks on 'Continue' button
	Then User see Password field
	When User enters invalid password
	When User clicks on 'Login' button
	Then User see error message under password field

