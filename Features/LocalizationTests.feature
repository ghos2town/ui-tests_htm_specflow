﻿Feature: Localization Tests

@LocalizationTests
Scenario: CheckRUSVersion
    Given User is on the Promo page
	When User clicks on the 'Start' button
	Then User should be redirected to 'Main' page


Scenario: CheckENVersion
	Given User is on the Promo page
	When User clicks on the 'Start' button
	When User clicks on the Language dropdown
	When User selects the EN language
	Then EN language is selected


Scenario: CheckUAVersion
	Given User is on the Promo page
	When User clicks on the 'Start' button
	When User clicks on the Language dropdown
	When User selects the UA language
	Then UA language is selected
	
