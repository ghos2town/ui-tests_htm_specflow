﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_SpecFlowProject.Drivers
{
    public class DriverUrl
    {
        public string Url { get; set; }

        public DriverUrl()
        {
            Url = "D:/Drivers/geckodriver"; //https://github.com/mozilla/geckodriver/releases/
        }
    }
}
