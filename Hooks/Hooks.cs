﻿using BoDi;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Diagnostics;
using TechTalk.SpecFlow;
using Test_SpecFlowProject.Drivers;

namespace Test_SpecFlowProject.Hooks
{
    [Binding]
    public sealed class Hooks
    {
        private IWebDriver driver;

        private readonly IObjectContainer _container;

        public Hooks(IObjectContainer container)
        {
            _container = container;
        }

        [BeforeScenario]
        private void BeforeScenario()
        {
            var driverurl = new DriverUrl();
            driver = new FirefoxDriver(driverurl.Url);
            driver.Navigate().GoToUrl("https://howtomove.ilavista.tech/ru/promo");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);

            _container.RegisterInstanceAs(driver);
        }


        [AfterScenario]
        private void AfterScenario()
        {
            if (_container.IsRegistered<IWebDriver>())
            {
                var driver = _container.Resolve<IWebDriver>();
                driver.Quit();
            }
        }
       
    }
}